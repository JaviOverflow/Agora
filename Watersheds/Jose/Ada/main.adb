--basic I/O
with Ada.Text_IO;                   use Ada.Text_IO;
with Ada.Integer_Text_IO;           use Ada.Integer_Text_IO;
with ada.strings.unbounded;         use ada.strings.unbounded;
with ada.strings.unbounded.Text_IO; use ada.strings.unbounded.Text_IO;


procedure main is 

    --constants
    BUID : constant character := character'last;
    

    --declaracions de tipus
    subtype altitud is integer range -1 .. 10;
    --subtype altitud is integer range 0 .. 10_000;
    type posicio is record x, y : natural; end record;
    type map_alt is array (integer range <>, integer range <>) of altitud;
    type map_bas is array (integer range <>, integer range <>) of character;
    type map_pos is array (integer range <>, integer range <>) of posicio;

    --variables de programa
    nMap : natural;
    H, W : natural;


    --variables de suport
    inp      : unbounded_string;  --entrada de teclat
    f, f_out : file_type;         --fitxer i/o
    c        : character;         --bassal


    --veure si existeix un fitxer
    function existeix (s : in string) return boolean is
        f : file_type;
    begin
        open(f, in_file, s);
        close(f);
        return true;
    exception
        when name_error => return false;
    end existeix;

begin

    --inicialització
    inp := to_Unbounded_String("input.txt");                       --selecció del fitxer de input
    --put(" Nom de fitxer: "); inp := get_line(standard_input);
    open(f, in_file, to_String(inp));
    
    get(f, nMap);                                                  --obtenció de les "constants"

    for map in 1 .. nMap loop
    
        get(f, H); get(f, W);
        
        declare --obtenció de memòria per a les dades
            
            type map_sol is record
                ma : map_alt(-1 .. H, -1 .. W) := (-1 .. H => (-1 .. W => -1)); --posar rang
                mb : map_bas(-1 .. H, -1 .. W) := (-1 .. H => (-1 .. W => BUID));
            end record;
            type data is record a : altitud; p : posicio; end record;  
            subtype index_data is positive range 1 .. H * W;
            type map_dat is array (index_data) of data;
            subtype index_x is natural range  0 ..  H;
            subtype index_y is integer range -1 .. W-1;
            ent : map_dat;
            sol : map_sol;
            x0 : index_x := 0;
            y0 : index_y := -1;
            swap : data;

            procedure push (v : in out map_dat; k, n : in index_data) is
                i, lcj : index_data;
                swap0 : data; 
                function "<" (x, y : in data) return boolean is
                begin
                    if x.a = y.a then 
                        if x.p.x = y.p.x then
                            if x.p.y = y.p.y then 
                                return false;
                            end if;
                            return x.p.y < y.p.y;
                        end if;
                        return x.p.x < y.p.x;
                    end if;
                    return x.a < y.a;
                end "<";
            begin
                if k <= n/2 then --és node interior (aka. te almenys un fill)
                    swap0 := v(k);         --guarda el swap
                    i := k; lcj := 2 * i; --ajusta els indexos
                    if lcj < n and then v(lcj+1) < v(lcj) then lcj := lcj + 1; end if;  --si hi ha dret i és menor, ajusta.. lcj = fill minim
                    while v(lcj) < swap0 loop
                        v(i) := v(lcj); i := lcj;  --pujar els menors que swap guardant la posició en que m'he quedat
                        exit when lcj > n/2;       --és un node fulla
                        lcj := 2 * i;
                        if lcj < n and then v(lcj+1) < v(lcj) then lcj := lcj + 1; end if;
                    end loop;
                v(i) := swap0;--guard el mínim al node
                end if;
            end push;   
           
            procedure visita(b : in data) is 
                x : natural renames b.p.x;
                y : natural renames b.p.y;
                a : altitud renames b.a;
            begin
                --mantenir al bassal que pertoqui
                if sol.mb(x, y) = BUID or  --si el meu bassal es buid ò
                   sol.ma(x+1, y)  = a or  --cap dels veïnats te la mateixa altura 
                   sol.ma(x-1, y)  = a or 
                   sol.ma (x, y+1) = a or 
                   sol.ma (x, y-1) = a then  
                          c := character'succ(c); --ajusta bassal actual..
                          sol.mb(x, y) := c; 
                          sol.ma(x, y) := a;
                end if;
                --put_line(sol.ma(x+1, y)'img);
                --si un veinat està buid, contagiar
                if sol.mb(x+1, y ) = BUID then sol.mb(x+1, y) := sol.mb(x, y); end if;--sol.ma(x+1, y) := a; end if;
                if sol.mb(x-1, y ) = BUID then sol.mb(x-1, y) := sol.mb(x, y); end if;--sol.ma(x-1, y) := a; end if;
                if sol.mb( x, y+1) = BUID then sol.mb(x, y+1) := sol.mb(x, y); end if;--sol.ma(x, y+1) := a; end if;
                if sol.mb( x, y-1) = BUID then sol.mb(x, y-1) := sol.mb(x, y); end if;--sol.ma(x, y-1) := a; end if;
            end visita;

        begin
            c := '`';
            for alt in reverse (index_data'last/2 + 1) .. index_data'last loop   --HeapSort: col·locar fulles
                --alt : posició en l'array, convé que col·loqui de adarrera cap envant
                --x0, y0 : posicions que es van llegint, comença desde la primera (0, 0)
                y0 := y0 + 1;
                get(f, ent(alt).a); --guarda la altura
                ent(alt).p.x := x0; --guarda la posició
                ent(alt).p.y := y0;
                if y0 = index_y'last then 
                    x0 := x0 + 1;
                    y0 := index_y'first; 
                end if;
            end loop;
            for alt in reverse 1 .. index_data'last/2 loop                --HeapSort: generar monticle
                --alt : important que vagui fent els monticles
                --x0, y0 : han de seguir d'on ho havien deixat
                y0 := y0 + 1;
                get(f, ent(alt).a); --guarda la altura
                ent(alt).p.x := x0; --guarda la posició
                ent(alt).p.y := y0;
                if y0 = index_y'last then 
                    x0 := x0 + 1;
                    y0 := index_y'first; 
                end if;
                push(ent, alt, index_data'last);
            end loop;
            for i in reverse 2 .. index_data'last loop               --HeapSort: monticle -> ordenat
                swap := ent(1);     --guarda el primer
                ent(1) := ent(i);   --posa el darrer el primer
                ent(i) := swap;     --col·loca el mín/máx del monticle a el darrer  
                push(ent, 1, i-1);  --torna a ajustar el montícle amb un element menys
            end loop;
            for bassal in reverse index_data loop
                visita(ent(bassal));
            end loop;
            for h in sol.mb'first(1)+1 .. sol.mb'last(1)-1 loop
                for w in sol.mb'first(2)+1 .. sol.mb'last(2)-1 loop
                    put(sol.mb(h, w)& ' ');
                end loop;
                new_line;
            end loop;
            new_line;
        end;
    end loop;

end main;
