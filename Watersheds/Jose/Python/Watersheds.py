#!/usr/bin/python3

def neightbours(cell):
    (x, y) = cell
    adjacents = [(0, -1), (+1, 0), (0, +1), (-1, 0)]
    for (dx, dy) in adjacents:
        yield (x+dx, y+dy)

def find_lowest_neightbour(d, cell):
    actual_neightbours = {x: d[x] for x in neightbours(cell) if x in d}
    return min( actual_neightbours, key=d.get )

def visit(sol, raw_map, cell):
    global c
    if cell not in sol:
        lcell = find_lowest_neightbour(raw_map, cell)
        if raw_map[lcell] < raw_map[cell]:
            sol[cell] = sol.setdefault(lcell, visit(sol, raw_map, lcell) )
        else:
            sol[cell] = chr(c)
            c = c + 1
    return sol[cell]

def printf(d, x, y):
    for x0 in range(x):
        for y0 in range(y):
            print(d[(x0,y0)], end=' ', file=fout)
        print(file=fout)

file_in  = input("Name of the input file: ")
file_out = "out_" + file_in

fin  = open(file_in,  'r')
fout = open(file_out, 'w')

N = int(fin.readline())

for bassin in range(N):

    #dim input
    x, y = map(int, fin.readline().split())

    #segur millorable: parse
    raw_map = {}
    for row in range(x):
        line = fin.readline().split(' ')
        for col in range(y):
            raw_map[(row, col)] = int(line[col]) #line[col]

    #processar input
    sol = {}
    c = 97 #'a'
    for x0 in range(x):
        for y0 in range(y):
            visit(sol, raw_map, (x0, y0))

    #mostrar output
    printf(sol, x, y)
