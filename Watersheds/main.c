#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MAX_L 16
#define MAX_H 11
#define MAX_BASINS 2
#define NONE '\0'

int height[MAX_L][MAX_L]; // matriz de altitudes
char label[MAX_L][MAX_L]; // matriz de etiquetas

char is_labeled(char label[MAX_L][MAX_L], int i, int j) {
    return label[i][j] != NONE;
}

void empty(char label[MAX_L][MAX_L], char * nlbl) {
    memset(label, NONE, MAX_L*MAX_L);
    *nlbl = 'a'-1;
}

void lowest_neighbour(int height[MAX_L][MAX_L], int w, int h, int i, int j, int * ci, int * cj) {
    int min=height[i][j];
    int caso = -1;

    if (i<h-1 && height[i+1][j] < min){
        min = height[i+1][j];
        caso = 2;
    }
    if (j<w-1 && ((caso != -1 && height[i][j+1] <= min) || height[i][j+1] < min)){
        min = height[i][j+1];
        caso = 3;
    }
    if (j>0 && ((caso != -1 && height[i][j-1] <= min) || height[i][j-1] < min)){
        min = height[i][j-1];
        caso = 1;
    }
    if (i>0 && ((caso != -1 && height[i-1][j] <= min) || height[i-1][j] < min)){
        min = height[i-1][j];
        caso = 0;
    }
    switch(caso) {
        case 0:
            *ci = i-1;
            break;
        case 1:
            *cj = j-1;
            break;
        case 2:
            *ci = i+1;
            break;
        case 3:
            *cj = j+1;
            break;
        default:
            break;
    }
}

void load_limits(FILE * f, int * w, int * h) {
    fscanf(f, "%d %d\n", h, w);
}

void load_map(FILE * f, int height[MAX_L][MAX_L], int w, int h) {
    int i, j;
    char * buf;
    char * token;
    size_t len=0;

    for(i=0; i<h; i++){
        getline(&buf, &len, f);
        token = strtok (buf, " ");
        for(j=0; j<w; j++) {
            sscanf(token, "%d", &height[i][j]);
            token = strtok (NULL, " ");
        }
    }
    free(buf);
}

void load_mnumber(FILE * f, int * t) {
    fscanf(f, "%d", t);
}

int main(int argc, char ** argv) {
    char nlbl; // siguiente etiqueta
    int t, w, h;
    int i, j, c = 0;
    int ci, cj;
    FILE * fin = fopen("in.txt", "r");
    FILE * fout = fopen("out.txt", "w");

    load_mnumber(fin, &t);
    for(c=0;c<t;c++) {
        load_limits(fin, &w, &h);
        load_map(fin, height, w, h);
        empty(label, &nlbl);

        for(i=0; i<h; i++) {
            for(j=0; j<w; j++) {
                ci = i; cj = j;
                lowest_neighbour(height, w, h, i, j, &ci, &cj);
                if (is_labeled(label, ci, cj)) {
                    label[i][j] = label[ci][cj];
                } else if(is_labeled(label, i, j)) {
                    label[ci][cj] = label[i][j];
                } else {
                    nlbl += 1;
                    label[i][j] = nlbl;
                    label[ci][cj] = nlbl;
                }
            }
        }

        fprintf(fout, "Case #%d:\n", c);
        for (i=0; i<h; i++) {
            for (j=0; j<w-1; j++) {
                fprintf(fout, "%c ", label[i][j]);
            }
            fprintf(fout, "%c\n", label[i][j]);
        }

    }
    fclose(fin);
    fclose(fout);
    return 0;
}
