#!/usr/bin/python3

def it(l, start, end):
    for x in range(end - 1, start - 1, -1):
        yield x, l[x]

file_in  = input("Name of the input file: ")
file_out = "out_" + file_in

fin  = open(file_in,  'r')
fout = open(file_out, 'w')

STR = 'welcome to code jam'
lstr = len(STR)
fin.readline() #consumir la primera línea

for ln, line in enumerate(fin):

    #create the data_structure
    states = [0] * (lstr + 1)
    nsr = 1
    states[0] = 1

    #deal with the input
    for c in line:
        for idx, val in it(states, 1, nsr+1):
            if c == STR[idx-1]:
                states[idx] += states[idx-1]
        if c == STR[nsr-1] and nsr < lstr:
            nsr += 1

    #save results
    print('Case #', ln + 1,':', str(states[lstr]), file=fout)
