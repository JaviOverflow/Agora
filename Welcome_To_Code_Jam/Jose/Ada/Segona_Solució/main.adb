--basic I/O
with Ada.Text_IO;                   use Ada.Text_IO;
with Ada.Integer_Text_IO;           use Ada.Integer_Text_IO;
with ada.strings.unbounded;         use ada.strings.unbounded;

procedure main is 

    --constants
    STR : constant string := "welcome to code jam";

    --declaracions de tipus
    type four_dig is mod 10_000;
    type aparicions is array (natural range 0 .. STR'last) of four_dig;

    --variables de programa
    estat : aparicions;

    --variables de suport
    inp      : unbounded_string;  --entrada de teclat
    f, f_out : file_type;         --fitxer i/o
    c : character;
    n : natural;

    --veure si existeix un fitxer
    function existeix (s : in string) return boolean is
        f : file_type;
    begin
        open(f, in_file, s);
        close(f);
        return true;
    exception
        when name_error => return false;
    end existeix;

begin

    --inicialització
    inp := to_Unbounded_String("input.txt");                       --selecció del fitxer de input
    --put(" Nom de fitxer: "); inp := get_line(standard_input);
    open(f, in_file, to_String(inp));

    get(f, n);                                                  --obtenció de les "constants"

    for line in 1 .. n loop

	estat := (others => 0);
	estat(0) := 1;

        get(f, c);
        while not end_of_line(f) loop
            get(f, c);
	    for ll in STR'range loop
		if c = STR(ll) then estat(ll) := estat(ll - 1) + estat(ll); end if;
		exit when estat(ll) = 0;
	    end loop;
        end loop;

	put_line("Case #"& line'img &":"& estat(STR'last)'img );
    end loop; --end reading lines

end main;
