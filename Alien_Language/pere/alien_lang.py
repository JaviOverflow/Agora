import sys

def skip_len(key, i):
    sk = i
    while key[sk] != ')':
        sk += 1
    return (sk)

class Trie(object):
    EOS = u'$'

    def __init__(self):
        super(Trie, self).__init__()
        self.root = {}

    def put(self, key):
        _key = key + self.EOS
        i = 0
        p = self.root
        c = _key[i]
        while c != self.EOS:
            if c not in p:
                p[c] = {}
            p = p[c]
            i += 1
            c = _key[i]

        if c not in p:
            p[c] = 1
        else:
            p[c] = p[c] + 1

    def value(self, key):
        _key = key + self.EOS
        return self._value(self.root, _key, 0, 1)

    def _value(self, p, key, i, last):
        if key[i] not in p:
            return 0

        p = p[key[i]]
        i = last
        c = key[i]
        count = 0
        while c != self.EOS:
            if c == '(':
                i += 1
                if key[i] != ')':
                    last = skip_len(key, i)

                    while i < last:
                        count += self._value(p, key, i, last+1)
                        i += 1

                    return count
                else:
                    i += 1

            elif c not in p:
                return 0

            else:
                p = p[c]
                i += 1
                c = key[i]

        if c == self.EOS and c in p:
            return p[c]
        else:
            return 0


if __name__ == '__main__':
    l = int(sys.argv[1])
    d = int(sys.argv[2]) + 4
    n = int(sys.argv[3]) + d

    trie = Trie()
    for index in range(4, d):
        trie.put(sys.argv[index])

    i = 0
    for index in range(d, n):
        print('Case #' + str(i) + ': ' + str(trie.value(sys.argv[index])))
        i += 1

