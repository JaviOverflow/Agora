#!/usr/bin/python3

def get_pattern(string):
    pattern = []; temp = pattern
    for c in string:
        if c == '(':
            pattern = []
        elif c == ')':
            temp.append(pattern)
            pattern = temp
        else:
            pattern.append(c)
    return pattern

def match(word, pattern):
    if len(word) != len(pattern): return 0
    for idx in range(len(word)):
        if word[idx] not in pattern[idx]: return 0
    return 1

file_in  = input("Name of the input file: ")
file_out = "out_" + file_in

fin  = open(file_in,  'r')
fout = open(file_out, 'w')

L, D, N = map(int, fin.readline().split(' '))

dict = []
for i in range(D):
    dict.append(fin.readline())

for line in range(N):
    raw_pattern = fin.readline()
    pattern = get_pattern(raw_pattern)
    matches = 0
    for word in dict:
        matches += match(word, pattern)
    print("Case #", line, ": ", matches, "\n", sep='', file=fout)

fout.close()
print('Results saved in:', file_out)
