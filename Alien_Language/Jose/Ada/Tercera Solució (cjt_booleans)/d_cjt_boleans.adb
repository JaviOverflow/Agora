package body d_cjt_boleans is

    procedure buida (c : out conjunt) is
        tex : taula_existencia renames c.tex;
    begin
        for k in element loop tex(k) := false; end loop;
    end buida;
    
    procedure posa  (c : in out conjunt; e : in element) is
        tex : taula_existencia renames c.tex;
    begin
        tex(e) := true;
    end posa;
    
    procedure elimina (c : in out conjunt; e : in element) is
        tex : taula_existencia renames c.tex;
    begin
        tex(e) := false;
    end elimina;
    
    function hi_es (c : in conjunt; e : in element) return boolean is
        tex : taula_existencia renames c.tex;
    begin
        return tex(e);
    end hi_es;

end d_cjt_boleans;
