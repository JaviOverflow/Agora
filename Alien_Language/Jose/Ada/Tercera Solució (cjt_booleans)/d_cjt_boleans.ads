generic
    type element is (<>);
package d_cjt_boleans is
    type conjunt is private;
    no_hi_es : exception;
    procedure buida   (c :    out conjunt);
    procedure posa    (c : in out conjunt; e : in element);
    procedure elimina (c : in out conjunt; e : in element);
    function hi_es    (c : in     conjunt; e : in element) return boolean;

private
    type taula_existencia is array (element) of boolean;
    type conjunt is record
        tex : taula_existencia;
    end record;
end d_cjt_boleans;
