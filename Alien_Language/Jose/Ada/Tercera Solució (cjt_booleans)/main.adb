--basic I/O
with Ada.Text_IO;                      use Ada.Text_IO;
with Ada.Integer_Text_IO;              use Ada.Integer_Text_IO;
with Ada.strings.unbounded;            use Ada.strings.unbounded;
with Ada.strings.unbounded.Text_IO;    use Ada.strings.unbounded.Text_IO;

--nous tipus
with d_cjt_boleans;

procedure main is 

    --declaracions de tipus
    subtype mem_lleng is string;                                 --array, set of words in dict.
    
    --subtype index_char is natural range 0 .. 31;                 --conjunt de caràcters possibles a una posició (variable)
    --package cjt_char is new d_set(character, index_char);   
    subtype index_char is character range 'a' .. 'z';
    package cjt_char is new d_cjt_boleans(index_char);
    use cjt_char;
    
    type word      is array (positive range <>) of cjt_char.conjunt; --conjunt de caràcters possibles
    --type word_iter is array (positive range <>) of iterator;


    --variables de programa
    L, D, N    : natural;
    inp, fname : unbounded_string;  --entrada de teclat
    f, f_out   : file_type;         --fitxer i/o
    
    
    --variables de suport
    c       : character;
    index, j, cont : natural;
    
    
    --veure si existeix un fitxer
    function existeix (s : in string) return boolean is
        f : file_type;
    begin
        open(f, in_file, s);
        close(f);
        return true;
    exception
        when name_error => return false;
    end existeix;

begin

    --inicialització
    inp := to_Unbounded_String("input.txt");                       --selecció del fitxer de input
    put(" Nom de fitxer: "); inp := get_line(standard_input);
    open(f, in_file, to_String(inp));
    
    get(f, l); get(f, d); get(f, n);                               --obtenció de les "constants"

declare

    --obtenció de memòria per a les dades
    subtype index_word is positive range 1 .. L;  --llargada de la paraula
    cjt_paraules : mem_lleng(1 .. L * D);         --llargada del diccionari                 
    w            : word (index_word);             --patró
    aux          : string (index_word);           --paraula a cercar si escau amb un estat del patró
begin
    
    inp := get_line(f);
    index := 1;
    for i in 1 .. D loop                                --obtenció de les paraules al diccionari
        inp := get_line(f);
        cjt_paraules(index .. index + L - 1) := to_String(inp);
        index := index + L;
    end loop;
    --put_line(cjt_paraules);
    
    create(f_out, out_file, "output.txt");
    for i in 1 .. N loop                                --per a cada patró
    
        for j in index_word loop                        --obtenir-lo per descriure el token, recorr una word
            buida( w(j) ); get(f, c);
            if c = '(' then                             --processar com a grup o com a símbol únic
                loop                                    --pot ser variable (més de dues possibilitats) OJO! d_set
                    get(f, c);                          
                    exit when c = ')';
                    posa(w(j), c);
                end loop;
            else 
                posa(w(j), c);
            end if;
        end loop;
        
        index := 1; cont := 0;
        for i in 1 .. D loop                             --cercar si la paraula escau al patró
            aux := cjt_paraules(index .. index + L - 1); --selecciona la paraula que li toca (i-paraula)
            j := 1; 
            loop                                         --no se usa "for" perque necessitam el valor de j al final
                exit when j > aux'last;
                exit when not hi_es( w(j), aux(j) );
                j := j + 1;
            end loop;
            if j > aux'last then cont := cont + 1; end if;
            index := index + L;
        end loop;
        
        put_line(f_out, "Case #"& i'img(2..i'img'last) &":"& cont'img);
    
    end loop;
    
    put_line(" S'ha desat la solució al fitxer output.txt");
    close(f); close(f_out);

exception
    when others =>
        close(f); close(f_out);
        put_line("Ha botat una excepció"); 
        put_line("S'ha detingut la aplicació"); raise;
end;
    
end main;
