package body d_set is

    --funcions d'ajuda privades
    type p_cond is access function(X, Y : element) return boolean;
    
    procedure cerca (s : in set; x : in element; trobat : out boolean; i : in out index) is--; f : in p_cond) is
        ne : cardinalitat  renames s.ne;
        m  : vector        renames s.m;
        --k  : cardinalitat;
    begin
        trobat := false;
        for j in index loop
            exit when j >= ne;
            if m(j) = x then
                trobat := true;
                i := j;
                exit;
            end if;
        end loop;
    end cerca;
    
    --funcions públiques
    procedure buida (s : out set) is                               --O(1)
        ne : cardinalitat  renames s.ne;
    begin
        ne := NUL;
    end buida;
    
    function es_buid (s : in set) return boolean is                --O(1)
        ne : cardinalitat  renames s.ne;
    begin
        return ne = NUL;
    end es_buid;
    
    procedure posa (s : in out set; x : in element) is             --O(n)
        ne : cardinalitat  renames s.ne;
        m  : vector        renames s.m;
        i  : index := index'first;
        t  : boolean;
    begin
        if ne = MAX then raise desbordament; end if;
        cerca(s, x, t, i);
        if t then raise ja_hi_es; end if;
        ne := ne + 1;
        m(ne) := x;
    end posa;
    
    procedure elimina (s : in out set; x : in element) is          --O(n)
        ne : cardinalitat  renames s.ne;
        m  : vector        renames s.m;
        i  : index := index'first;
        t  : boolean;
    begin
        cerca(s, x, t, i);--, "="'Access);
        if not t then raise no_hi_es; end if; --eliminar quan es buid controlat
        m(i) := m(ne);
        ne := ne - 1;
    end elimina;
    
    function hi_es (s : in set; x : in element) return boolean is  --O(n)
        i : index := index'first;
        t : boolean;
    begin
        cerca(s, x, t, i);--, "="'Access);
        return t;
    end hi_es;
    
    function consulta (s : in set; n : in cardinalitat) return element is
        ne : cardinalitat  renames s.ne;
        m  : vector        renames s.m;
    begin
        if n <= ne and n /= cardinalitat'first then return m(n); end if;
        raise no_hi_es;
    end consulta;
    
    procedure recorregut (s : in set; f : in p_proc) is
        ne : cardinalitat  renames s.ne;
        m  : vector        renames s.m;
        i  : index := index'first;
    begin
        while i <= ne loop f( m(i), i ); i := i + 1; end loop;
    end recorregut;
    
    
    --iterator
    procedure first (it : out iterator; s : in set) is
        ne : cardinalitat  renames s.ne;
    begin
        it := iterator(NUL + 1);
    end first;
    
    procedure n_pos (it : out iterator; n : in cardinalitat) is
    begin
        it := iterator(n);
    end n_pos;
    
    procedure next (it : in out iterator; s : in set) is
    begin
        if index(it) = MAX then raise mal_us; end if;
        it := it + 1;
    end next;
    
    function is_valid (it : in iterator; s : in set) return boolean is
        ne : cardinalitat  renames s.ne;
    begin
        return cardinalitat(it) < ne;
    end is_valid;
    
    function get      (it : in     iterator; s : in set)    return element is
        m  : vector        renames s.m;
    begin
        return m( index(it) );
    end get;
    
    
end d_set;
