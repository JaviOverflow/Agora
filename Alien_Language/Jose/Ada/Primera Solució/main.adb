--basic I/O
with Ada.Text_IO;                      use Ada.Text_IO;
with Ada.Integer_Text_IO;              use Ada.Integer_Text_IO;
with Ada.strings.unbounded;            use Ada.strings.unbounded;
with Ada.strings.unbounded.Text_IO;    use Ada.strings.unbounded.Text_IO;

--nous tipus
with d_set;

procedure main is 
    
    --declaracions de tipus
    subtype mem_lleng is string;                                 --array, set of words in dict.
    
    subtype index_char is natural range 0 .. 30;                 --conjunt de caràcters possibles a una posició (variable)
    package cjt_char is new d_set(character, index_char);   
    use cjt_char;
    
    type word      is array (positive range <>) of cjt_char.set;--conjunt de caràcters possibles
    type word_iter is array (positive range <>) of iterator;


    --variables de programa
    L, D, N   : natural;
    inp       : unbounded_string;  --entrada de teclat
    f, f_out  : file_type;         --fitxer i/o
    
    
    --variables de suport
    c   : character;
    index : natural := 1;
    
    
    --veure si existeix un fitxer
    function existeix (s : in string) return boolean is
        f : file_type;
    begin
        open(f, in_file, s);
        close(f);
        return true;
    exception
        when name_error => return false;
    end existeix;
    

    --consultar si hi ha alguna de les combinacions al diccionari
    procedure visita (dict : in mem_lleng; w0 : in word; t : out natural) is
        w_it  : word_iter (w0'range); --recorregut
        aux   : string    (w0'range); --estat del patró
        
        --generar l'estat a partir dels iteradors i el conjunt
        procedure obtenir_estat (str : out string) is --BigO(index_word);
        begin
            for i in w0'range loop
                str(i) := get(w_it(i), w0(i));
            end loop;
        end obtenir_estat;
        
        --consultar un estat al diccionari
        function hi_es (dict : in mem_lleng; s : in string) return boolean is
            --la mida de l'string es divideix la mida de mem_lleng
            MAX_DICT : constant natural := dict'last; --coincideix L*D
            MAX_WORD : constant natural :=    s'last; --coincideix L
            N_WORDS : constant natural := MAX_DICT / MAX_WORD;
            ini     : natural := 1;
        begin
            for i in 1 .. N_WORDS loop
                if dict(ini .. ini + MAX_WORD - 1) = s then return true; end if;
                ini := ini + MAX_WORD;
            end loop;
            return false;
        end hi_es;
        
        --genera les combinacions i en cas que alguna coincidesqui incrementa el contador
        procedure visita0 (w : in word) is  --BigO(cardinalitat*card*card...)
            n : natural := w'last;
        begin
            if n > 0 then                      --quan li toca seguir amb un tros més petit
                first(w_it(n), w(n));          --el posa desde 0
                visita0( w(w'first..n-1) );      --visita el seg més petit
                while is_valid( w_it(n), w(n) ) loop --si te germans també els mira
                    next(w_it(n), w(n));
                    visita0( w(w'first..n-1) );   --visita el seg més petit ara que està al germà
                end loop;
            --end if;                            --ja no hi ha trossos més petits i acaba de recórrer
            elsif n = 0 then                      --ha acabat de recorrer fins abaix el patró (crida de recursió concreta)
                obtenir_estat(aux);
                if hi_es(dict, aux) then t := t + 1; end if;
            end if;
        end visita0;
        
    begin
        t := 0; visita0(w0);
    end visita;
    
begin

    --inicialització
    inp := to_Unbounded_String("input.txt");                       --selecció del fitxer de input
    put(" Nom de fitxer: "); inp := get_line(standard_input);
    open(f, in_file, to_String(inp));
    
    get(f, l); get(f, d); get(f, n);                               --obtenció de les "constants"

declare

    --obtenció de memòria per a les dades
    subtype index_word is positive range 1 .. L;  --llargada de la paraula
    cjt_paraules : mem_lleng(1 .. L * D);         --llargada del diccionari                 
    w            : word (index_word);             --patró
    
begin
    
    inp := get_line(f);
    for i in 1 .. D loop                                --obtenció de les paraules al diccionari
        inp := get_line(f);
        cjt_paraules(index .. index + L - 1) := to_String(inp);
        index := index + L;
    end loop;
    
    create(f_out, out_file, "output.txt");
    for i in 1 .. N loop                                --per a cada patró
    
        for j in index_word loop                        --obtenir-lo per descriure el token, recorr una word
            buida( w(j) ); get(f, c);
            if c = '(' then                             --processar com a grup o com a símbol únic
                loop                                    --pot ser variable (més de dues possibilitats) OJO! d_set
                    get(f, c);                          
                    exit when c = ')';
                    posa(w(j), c);
                end loop;
            else 
                posa(w(j), c);
            end if;
        end loop;
        
        visita(cjt_paraules, w, index);                  --genera les combinacions i les cerca al diccionari
        
        put_line(f_out, "Case #"& i'img(2..i'img'last) &":"& index'img);
    
    end loop;
    
    put_line(" S'ha desat la solució al fitxer output.txt");
    close(f); close(f_out);

exception
    when others =>
        close(f); close(f_out);
        put_line("Ha botat una excepció"); 
        put_line("S'ha detingut la aplicació"); raise;
end;
    
end main;
