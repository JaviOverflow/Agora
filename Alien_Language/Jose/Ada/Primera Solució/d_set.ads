generic
    type element      is private;
    type cardinalitat is range <>;

package d_set is
    type set    is limited private;
    type p_proc is access procedure(X : element; n : cardinalitat);
    ja_hi_es     : exception;
    no_hi_es     : exception;
    desbordament : exception;
    procedure buida   (s :    out set);
    procedure posa    (s : in out set; x : in element);
    procedure elimina (s : in out set; x : in element);
    function es_buid  (s : in     set)                      return boolean;
    function hi_es    (s : in     set; x : in element)      return boolean;
    function consulta (s : in     set; n : in cardinalitat) return element;
    procedure recorregut (s : in set; f : in p_proc);
    
    type iterator is private;
    mal_us       : exception;
    procedure first   (it :    out iterator; s : in set);
    procedure n_pos   (it :    out iterator; n : in cardinalitat);
    procedure next    (it : in out iterator; s : in set);
    function is_valid (it : in     iterator; s : in set)    return boolean;
    function get      (it : in     iterator; s : in set)    return element;
    
private  
    NUL : constant cardinalitat := cardinalitat'first;
    MAX : constant cardinalitat := cardinalitat'last;
    subtype index is cardinalitat range NUL + 1 .. MAX;
    type vector is array (index) of element;
    type set is record
        ne : cardinalitat;
        m  : vector;
    end record;
    type iterator is new cardinalitat;
    
end d_set;
